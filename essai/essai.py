import lxml.etree as ET
import re

def decode_num_entities(s):
    return re.sub(r"&#(\d+);", lambda m: chr(int(m.group(1))), s)
    
dom = ET.parse("content.xml")
xslt = ET.parse("essai.xslt")
transform = ET.XSLT(xslt)
newdom = transform(dom)
print(decode_num_entities(ET.tostring(newdom, pretty_print=True).decode("utf-8")))
