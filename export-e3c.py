#! /usr/bin/python3

"""
Exporte un fichier .odt d'une série de QCM E3C vers un format
interactif. Première exportation implémentée : HTML
"""

import sys, argparse, odf.opendocument, odf.text, os
from subprocess import call
from e3c.qcm import *

def toiletteOdt(doc):
    """
    apporte des changements cosmétiques à un opendocument,
    en particulier ça augmente la taille de police dans les
    inserts texte. Les parties sans rapport avec un QCM sont 
    otées.
    @param doc une instance de odf.opendocument.OpenDocument
    @return une instance de odf.opendocument.OpenDocument, toilettée
    """
    TEXT='urn:oasis:names:tc:opendocument:xmlns:text:1.0'
    OFFICE='urn:oasis:names:tc:opendocument:xmlns:office:1.0'
    content=doc.topnode.childNodes[-1]
    body=content.childNodes[0]
    styles_synonymes=stylesQCM(doc)
    styles_qcm=set()
    for k,s in styles_synonymes.items():
        styles_qcm |= s
    ### On marque les éléments de body à conserver
    conserver=[]
    for elt in body.childNodes:
        # marquage des paragraphes, ce qui comprend aussi thèmes et sous-thèmes
        if elt.tagName=="text:p" and \
           elt.getAttrNS(TEXT, 'style-name')in styles_qcm:
            conserver.append(elt)
        # marquages des listes
        if elt.tagName=="text:list":
            paragraphes=elt.getElementsByType(odf.text.P)
            for p in paragraphes:
                if p.getAttrNS(TEXT, 'style-name')in styles_qcm:
                    conserver.append(elt)
                    # Il suffit d'un seul paragraphe correct, on garde la liste
                    break
    ### On supprime les éléments de body non marqués
    ### Depuis la fin vers le début
    for elt in body.childNodes[::-1] :
        if elt not in conserver:
            body.removeChild(elt)
    return


if __name__=="__main__":
    ################ analyse des arguments de la commande #########
    parser = argparse.ArgumentParser()
    parser.add_argument("odtFileName", help="Nom d'un fichier texte OpenDocument")
    parser.add_argument("-v", "--verbose", help="Force un rapport verbeux",
                        action="store_true")
    parser.add_argument(
        "--to", metavar="format",
        help="Précise le format d'exportation : 'html', etc."
    )
    parser.add_argument(
        "-o", "--output-dir", metavar="dossier",
        default="out",
        help="Précise le répertoire d'exportation ('out' par défaut)"
    )
    args = parser.parse_args()
    ################################################################
    
    doc = odf.opendocument.load(args.odtFileName)
    
    ################## test de validité ############################
    OK, lesQCM, deja, message = recolteQCM(doc)
    if not OK:
        sys.stderr.write(">>>>>>>>>>>>>>>>>>> erreur <<<<<<<<<<<<<<<<<<<<<<<\n")
        sys.stderr.write(message+"\n")
        if args.verbose:
            sys.stderr.write(">>>>>>>>>>>>>>>> texte déjà analysé <<<<<<<<<<<<<<\n")
            for l in deja:
                sys.stderr.write(l+"\n")
        else:
            sys.stderr.write("ajouez l'option -v pour agmenter la verbosité si vous voulez\n")
        sys.exit(-1)
    ######### le fichier args.odtFileName est maintenant validé #####
    fname=os.path.basename(args.odtFileName)
    prefix_html = re.sub(r'\.odt$', '_html', fname)
    fname_html = re.sub(r'\.odt$', '.html', fname)
    fname_html_tmp = re.sub(r'\.odt$', '.tmp.html', fname)
    toiletteOdt(doc) # toilette le document
    # puis on en enregistre la version toillettée sous args.output_dir
    call(f"mkdir -p {args.output_dir}", shell=True)
    doc.save(os.path.join(args.output_dir, fname))
    # on exporte ça en HTML à la façon d'OpenOffice
    # ce qui a l'avantage de calculer les attributs de style
    # de convertir les formules en images, et de gérer toutes les
    # illustrations. Avant la conversion, on efface les fichiers
    # éventuellement issus de conversions précédentes
    cmd=f"(cd {args.output_dir}; rm -f {prefix_html}*; lowriter --convert-to html {fname})"
    call(cmd, shell=True)
    # Enfin, on retravaille le code HTML
    doc=openHtml(os.path.join(args.output_dir, fname_html))
    qcmizeHtml(doc)
    with open(os.path.join(args.output_dir, fname_html_tmp),"w") as outfile:
        outfile.write(str(doc))
    cmd=f"(cd {args.output_dir}; mv {fname_html_tmp} {fname_html})"
    call(cmd, shell=True)
    print(f"on a écrit et rendu interactif : {os.path.join(args.output_dir, fname_html)}")
    
