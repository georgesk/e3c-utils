import odf.opendocument, re, os
from copy import deepcopy

from xml.dom import minidom
from bs4 import BeautifulSoup

class QCM:
    """
    Une classe qui contient tout le nécessaire pour une question à choix
    mutiples avec quatre réponses, à savoir :
    le nom d'un thème, celui d'un sous-thème, 
    une liste de paragraphes d'énoncé, 
    une liste de quatre listes de paragraphes de réponses
    une section de métadonnées : numéro de bonne réponse, difficulté estimée
    """

    def __init__(self, theme, sousTheme):
        self.theme=theme
        self.sousTheme=sousTheme
        self.enonce=[]
        self.reponse=[[],[],[],[],]
        self.bonne=None
        self.difficulte=None
        return

    def __str__(self):
        forme="""QCM
  Thème      = {theme}
  Sous-thème = {sousTheme}
  =========== Énoncé =====================
  {enonce}
  =========== Réponses ===================
  1 - {r1}
  2 - {r2}
  3 - {r3}
  4 - {r4}
  ========================================
  Bonne réponse = {bonne}
  Difficulté    = {difficulte}
"""
        return forme.format(
            theme=self.theme,
            sousTheme=self.sousTheme,
            enonce="\n".join([p.toxml() for p in self.enonce]),
            r1="\n".join([p.toxml() for p in self.reponse[0]]),
            r2="\n".join([p.toxml() for p in self.reponse[1]]),
            r3="\n".join([p.toxml() for p in self.reponse[2]]),
            r4="\n".join([p.toxml() for p in self.reponse[3]]),
            bonne=self.bonne,
            difficulte=self.difficulte,
        )

    
    def render(self, doc, which="enonce", to="html"):
        """
        moteur de rendu du qcm
        @param doc une instance odf.OpenDocument qui contient styles et images
        @param which la partie du qcm à rendre ; "enonce" par défaut
        @param to la cible du rendu ; "html" par défaut
        pour le moment, seule la cible "html" est implémentée.
        @return une chaîne de caractères
        """
        """
        DÉVELOPPEMENT EN COURS !!!
        """
        from jinja2 import Environment, PackageLoader

        env = Environment(loader=PackageLoader('e3c', 'templates'))
        if to=="html":
            if which=="enonce":
                template = env.get_template('enonce.html')
                result=template.render(enonce=self.enonce, toHtml=self.toHtml, doc=doc)
                return result
        return ""

    @staticmethod
    def toHtml(elt, doc, root=None):
        """
        portage des éléments de odf.OpenDocument vers HTML
        """
        import xml.dom
        if elt.nodeType==3: #TextNode
            root.appendChild(root.createTextNode("grrr"))
            return
        if elt.tagName=="text:p":
            result=minidom.parseString("<p/>")
            for n in elt.childNodes:
                QCM.toHtml(n, doc, result.documentElement)
            if root==None:
                return result.toxml().replace('<?xml version="1.0" ?>','')
            else:
                root.appendChild(result)
                return
        else:
            return elt.toxml()
    
def stylesQCM(doc):
    """
    Renvoie tous les noms de styles qui sont apparentés à des styles
    attachés au QCM.
    @param doc un document, instance de odf.opendocument.OpenDocument
    @return un dictionnaire d'ensembles de styles, les clés étant les styles
    attachés au QCM
    """
    ### on sérialise l'opendocument et on le rouvre sous xml.dom.minidom
    doc=minidom.parseString(doc.contentxml().decode("utf-8"))
    
    result={}
    ### on récolte tous les alias de styles QCM-*
    for elt in doc.getElementsByTagName("style:style"):
        for k, v in elt.attributes.items():
            if re.match(r"QCM.*", v):
                name=elt.getAttribute("style:name")
                if v in result:
                    result[v].add(name)
                else:
                    result[v]={v,name}
    ### puis on récolte tous les styles QCM-* utilisés dans des paragraphes
    for elt in doc.getElementsByTagName("text:p"):
        for k, v in elt.attributes.items():
            if re.match(r"QCM.*", v):
                if v not in result:
                    result[v]={v}
    return result

def estStyleQCM(elt, synonymes):
    """
    vérifie si un élément possède un style lié au QCM
    @param elt un élément instance de xml.dom
    @param synonymes un dictionnaire nom => ensemble de synonymes de styles
    @return None ou le nom canonique d'une style s'il a été trouvé
    """
    s=elt.getAttribute("text:style-name")
    for k, ens in synonymes.items():
        if s in ens:
            return k
    return None

def textePlat(elt, deja=""):
    """
    Renvoie le texte « aplati » d'un élément, instance de xml.dom
    en concaténant tous les textNodes
    @param elt : un élément de xml.dom
    @param deja le texte déjà capturé
    """
    if elt.nodeValue:
        return deja+elt.nodeValue
    else:
        for n in elt.childNodes:
            deja=textePlat(n,deja)
        return deja

def recolteQCM(doc):
    """
    Parcourt un texte et récolte les QCM qui s'y trouvent ;
    la méthode utilisé est la suivante :

    on parcourt tous les descendants directs de l'élément office:text
    qui sont soit du type text:p avec un style QCM-* soit du type
    text:list -> text:list-item -> text:p avec un style QCM-reponse
    chaque fois qu'un paragraphe de style QCM-Theme est rencontré,
      on change de thème courant,
      si on n'est pas en train de traiter une question de QCM, on
      peut considérer des paragraphes de style QCM-sous-theme
      on attend une séquence de paragraphes de styles
         QCM-enonce puis QCM-reponse puis QCM-metadonnees
    
    @param doc une instace de odf.OpenDocument
    @return (ok, lesQCM, deja, message) quatre valeurs qui signifient :
    - ok : vrai si le fichier a été validé
    - lesQCM : un dictionnaire thème => QCM avec les résultats
    - deja : un texte plat de ce qui a pu être analysé sans faute
    - message : un message d'erreur
    """
    content=minidom.parseString(doc.contentxml().decode("utf-8"))
    styles_synonymes=stylesQCM(doc)
    contentText=content.getElementsByTagName("office:text")[0]
    OK=True
    deja=[]
    message=""
    estEnCoursQuestion=False # True quand on en est à récolter une question
    lesQCM = {} # un dictionnaire thème => liste de QCM
    themeCourant=None
    sousThemeCourant=None
    qcmCourant=None
    for c in contentText.childNodes:
        ## boucle sur tous les fils de "office:text"
        ok=estStyleQCM(c,styles_synonymes)
        if ok:
            if ok=="QCM-Theme":
                if estEnCoursQuestion:
                    message=f"Un thème ne doit pas survenir tant qu'on n'a pas fini de définir un QCM : « {textePlat(c)} »"
                    OK=False
                    break;
                themeCourant=textePlat(c)
                if themeCourant not in lesQCM:
                    # on crée une liste par thème
                    lesQCM[themeCourant]=[]
                sousThemeCourant=None
            elif ok=="QCM-sous-theme":
                if estEnCoursQuestion:
                    message = f"Un sous-thème ne doit pas survenir tant qu'on n'a pas fini de définir un QCM : « {textePlat(c)} »"
                    OK=False
                    break
                sousThemeCourant=textePlat(c)
            elif ok=="QCM-enonce":
                estEnCoursQuestion=True
                if not qcmCourant:
                    qcmCourant=QCM(themeCourant, sousThemeCourant)
                qcmCourant.enonce.append(c)
            elif ok=="QCM-metadonnees":
                if not estEnCoursQuestion or not qcmCourant:
                    message=(f"Ces métadonnées ne sont pas correctement placées : « {textePlat(c)} »")
                    OK=False
                    break
                m=re.match(r".*OK\s*=\s*([\d]+)\s*,\s*niveau\s*=\s*(.*)",textePlat(c))
                if not m:
                    message=(f"La syntaxe des métadonnées est incorrecte: « {textePlat(c)} »")
                    OK=False
                    break
                qcmCourant.bonne=int(m.group(1))
                qcmCourant.difficulte=m.group(2)
                lesQCM[themeCourant].append(qcmCourant)
                qcmCourant=None
                estEnCoursQuestion=False
        elif c.tagName=="text:list":
            index=1
            # on parcout les enfants text:list-item et les petits-enfants text:p
            for li in c.getElementsByTagName("text:list-item"):
                for p in li.getElementsByTagName("text:p"):
                    ok=estStyleQCM(p,styles_synonymes)
                    if ok:
                        ## on tient une réponse : reste à vérifier que le
                        ## contexte est correct
                        if not estEnCoursQuestion or not qcmCourant:
                            message=f"Impossible d'insérer une réponse à ce niveau : « {textePlat(c)} »"
                            OK=False
                            break
                        qcmCourant.reponse[index-1].append(p)
                index+=1
        ## on est à la fin de la boucle sur les noeuds fils de "office:text"
        ## si on arrive ici c'est qu'il n'y a pas eu de break, pas d'erreur
        deja.append(textePlat(c))
    return OK, lesQCM, deja, message

def toHtml(lesQCM, doc, filename):
    """
    @param lesQCM un dictionnaire thème => QCM avec les résultats
    @param doc une instace de odf.OpenDocument
    @param filename le nom du fichier ODT, sert de modèle pour le fichier HTML
    """
    from jinja2 import Environment, PackageLoader
    from sys import stderr
    from subprocess import call
    
    env = Environment(loader=PackageLoader('e3c', 'templates'))
    targetFileName=re.sub(r'\.odt', '.html', filename)
    targetDirName=re.sub(r'\.odt', '_files', filename)
    content="" # le contenu principal : QCMs de la page web
    for t,qs in lesQCM.items():
        for qcm in qs:
            template = env.get_template('un_qcm.html')
            content+=template.render(qcm=qcm)
    with open(targetFileName,"w") as outfile:
        template = env.get_template('qcm.html')
        outfile.write(template.render(content=content))
    ### envoie les images dans un sous-dossier de targetDirName
    cmd=f"mkdir -p {targetDirName}/Pictures"
    call(cmd, shell=True)
    for imagePath in doc.Pictures:
        _,image,_ = doc.Pictures[imagePath]
        pathToImage=f"{targetDirName}/{imagePath}"
        with open(pathToImage,"wb") as outImageFile:
            outImageFile.write(image)
    ### les images sont sous {targetDirName}/Pictures
    stderr.write(f"le fichier {targetFileName} et le dossier {targetDirName}/ sont créés\n")
    return

def openHtml(path):
    """
    Crée une instance minidom pour le fichier html donné
    @param path un chemin vers un fichier html
    @return une instance de bs4.BeautifulSoup
    """
    return BeautifulSoup(open(path,"r"), "lxml")

def qcmizeHtml(doc):
    """
    Transforme un document HTML en page interactive pour que les
    QCM fonctionnent
    @param doc une instance de bs4.BeautifulSoup
    """
    ## injection d'un style spécifique
    style = doc.head.style
    lines=style.string.split("\n")
    newlines=[l for l in lines if not l.strip().startswith("p.qcm")]
    css_addonfile=os.path.join(os.path.dirname(__file__), "_static", "qcm.css")
    style.string="\n".join(newlines)+open(css_addonfile).read()
    # on ajoute le javascript
    scriptfile=os.path.join(os.path.dirname(__file__), "_static", "qcm.js")
    script=doc.new_tag("script")
    script.string=open(scriptfile).read()
    doc.head.append(script)
    ## on donne une classe "undef" aux éléments des listes
    ## et on les rend cliquables
    for li in doc.find_all("li"):
        li["class"]="undef"
        li["title"]="Cliquer pour sélectionner"
        li["onclick"]="choixReponse(this)"
    # on ajoute les boutons pour oublier les réponses
    i=0
    for ol in doc.find_all("ol"):
        ol["data-button-id"]="button"+str(i)
        button=doc.new_tag("button")
        button.string="Oublier la réponse choisie"
        button["onclick"]="Oubli(this)"
        button["class"]="hidden"
        button["id"]="button"+str(i)
        i+=1
        ol.insert_after(button)
    # on ajoute la boîte pour valider les réponses
    boite=BeautifulSoup("<div id='valider' onclick='Noter()'>Valider les réponses<br/>et évaluer le tout<div id='resultat'></div></div>", features="lxml")
    doc.body.append(boite)
    # on fusionne les paragraphes voisins en commençant par les derniers
    """
    ### PAS ENCORE AU POINT !!!!
    paragraphes=doc.find_all("p")[::-1]
    i=0
    while i < len(paragraphes)-1:
        p=paragraphes[i]
        p1=paragraphes[i+1]
        if "class" in p.attrs and "class" in p1.attrs and \
           "qcm-enonce" in p["class"] and "qcm-enonce" in p1["class"]:
            tmp=p.extract()
            p1.append(doc.new_tag("br"))
            p1.append(doc.new_tag("br"))
            for elt in tmp.contents:
                p1.append(elt)
        i+=1
    """
    return
    
    
