
/**
 * fonction de rappel pour quand on clique sur une réponse : cela modifie
 * les attributs de l'élément, donc son apparence
 * @param elt l'élément "li" qui a été cliqué
 **/
function choixReponse(elt){
    var ol = elt.parentElement;
    var buttonId = ol.getAttribute("data-button-id");
    var items = ol.getElementsByTagName("li");
    for (var i=0; i < items.length; i++){
	var li = items[i];
	li.className="unchecked";
    }
    elt.className="checked";
    // on rend le bouton qui suit "ol" visible
    var button=document.querySelector("button#"+buttonId);
    button.className="visible";
}

/**
 * fonction de rappel pour oublier les réponses précédemment cliquées
 * @param elt l'élément "button" qui a été cliqué
 **/
function Oubli(elt){
    var id=elt.getAttribute("id");
    var ols = document.querySelectorAll("ol");
    var ol;
    for(var i=0; i<ols.length; i++){
	if (ols[i].getAttribute("data-button-id")==id){
	    ol=ols[i];
	}
    }
    var items=ol.getElementsByTagName("li");
    for (var i=0; i < items.length; i++){
	var li = items[i];
	li.className="undef";
    }
    elt.className="hidden";
}
/**
 * fonction de rappel pour évaluer le QCM
 **/
function Noter(){
    var questions=document.querySelectorAll("ol");
    var meta=document.querySelectorAll("p.qcm-metadonnees");
    re=/.*OK\s*=\s*(\d+)\s*,\s*.*/; // pour trouver la bonne réponse
    var score=0
    var bonnes = 0;
    var mauvaises = 0;
    var nd = questions.length;
    var max=3*questions.length;
    for (var i=0; i < questions.length; i++) {
	var q=questions[i];
	console.log(meta[i].innerHTML.match(re)[1]);
	var bonne=parseInt(meta[i].innerHTML.match(re)[1]); // GRRR
	console.log(q.querySelector("input"));
	var items=q.querySelectorAll("li");
	var choix=0; // indéfini
	for (var j=0; j < 4; j++){
	    var li=items[j];
	    if (li.className=="checked"){
		choix=j+1;
		break;
	    }
	}
	console.log("bonne, choix", bonne, choix)
	if (choix>0) {                   // rien pour une réponse indéfinie
	    nd -= 1;
	    if (choix==bonne) {
		score +=3;              // trois points pour une bonne réponse
		bonnes +=1;
	    }
	    else {
		score-=1;               // moins un pour une mauvaise
		mauvaises +=1;
	    }
	}
    }
    var texte = "Sur les "+questions.length+" questions, il y a "+
	nd+" questions restées sans réponse, "+
	bonnes+" bonnes réponses, "+
	mauvaises+" réponses fausses. Le score est : "+score+"/"+max;
    var feedback=document.querySelector("div#resultat");
    feedback.innerHTML=texte;
    feedback.className="visible";
}
