E3C-UTILS
=========

Des utilitaires pour gérer des questionnaires à choix multiples, créé
par plusieurs collaborateurs.

Ce travail est lié au développement de questionnaires à choix multiples,
destinés aux élèves suivant la spécialité NSI (Numérique et Sciences
de l'Information) à partir de 2019, au niveau de la classe de première
de lycée, et qui ne souhaitent pas continuer cette spécialité en classe
de terminale. Dans ce cas, ils subissent une épreuve de contrôle continu,
qui comptera pour le baccalauréat.

Il s'agit d'une série de 42 questions, sélectionnées à partir d'une base
nationale, régionale, ou moins grande, dont chacune permet de choisir entre
quatre réponses exactement.

Pour chaque question, les règles sont :

*  il y a quatre réponses proposées au candidat
*  une seule des réponses est juste, les autre sont fausses
*  le candidat peut choisir de ne pas répondre

Le calcul de la note se fait selon la formule :

**trois fois le nombre de réponses justes, moins une fois le nombre de réponses fausses.**

Autrement dit, une bonne réponse apporte trois points, une mauvaise en retire un, et l'absence de réponse n'ajoute rien, mais ne retire rien non plus.

Méthode pour la collaboration
-----------------------------

Les fichiers où l'on propose les QCM sont au format OpenDocument, et ce sont
**obligatoirement** des dérivés d'un fichier tel que le fichier 
``odt-files/e3c-modele.odt`` : il est indispensable de disposer des mêmes
**styles spéciaux**, qui sont utilisés pour **marquer** les parties
pertinentes des questions.

### Comment se mettre au travail : ###

On peut partir d'un de ses propres fichier, mais tous doivent descendre
du fichier ``odt-files/e3c-modele.odt`` pour disposer des mêmes noms de
styles exactement, sinon le *validateur* ne pourra pas assurer que les
travaux sont inter-opérables, ni qu'on peut retravailler automatiquement les
sujets pour les utiliser dans diverses plates-formes interactives ou les
imprimer sur papier de façon consistante, en évitant toute saisie
supplémentaire.

Une méthode consiste à remplacer les parties existantes dans un document-modèle
par son travail, en respectant les styles suivants :

*  **QCM-Theme** pour les titres de niveau 1 qui précèdent une question ; par exemple, ce style doit être utilisé pour le titre « Représentation des données : types et valeurs de base » ;
*  **QCM-sous-theme** pour les titres de niveau 2, qui sont moins nécessaires ; par exemple, « Représentation binaire d’un entier relatif » peut être utilisé avec ce style ;
*  **QCM-enonce** est le style obligatoire pour toutes les lignes de l'énoncé ; l'énoncé peut comporter des éléments insérés, comme des images, ou des textes (boîtes de texte, par exemple pour des lignes de code) : il est important dans ce cas que l'élément inséré soit ancré *au paragraphe* dans un des paragraphes de l'énoncé, et qu'il n'y ait pas d'adaptation du texte (clic droit, Adaptation du texte --> pas d'adaptation). Si un élément n'est pas ancré dans l'énoncé, il ne sera pas exporté avec l'énoncé.
*  **QCM-reponse** est le style obligatoire pour chacune des quatre réponses proposées. Celles-ci seront dans une *liste numérotée*. Là aussi, il est possible d'intégrer des images et/ou des boîtes de texte, avec les mêmes précautions que ce qui est écrit plus haut.
*  **QCM-metadonnees** est le style obligatoire, pour une ligne de texte qu'on place **après la liste des réponses** ; cette ligne possède une syntaxe obligatoire : ``OK = <un chiffre de 1 à 4>, niveau = <facile, moyen, difficile>``. Le vérificateur est attentif à la présence de cette ligne, qui est nécessaire au traitement automatique.

Que faire des fichiers ODT, une fois qu'on les a ?
--------------------------------------------------

Il est prévu qu'on puisse les rassembler dans un lieu de stockage
officiel et sécurisé. En attendant, ils restent bien sûr dans nos
archives personnelles.

Vérifier que les questionnaires sont interopérables
===================================================

Le script ``export-e3c.py`` permet de faire deux choses :

*  validation d'un questionnaire ; en cas d'erreur, un message
   signale où celle-ci est survenue et donne des renseignements utiles ;
*  export sous forme d'un questionnaire web interactif (peu
   sécurisé), qui permet de tester facilement si on veut.
   
Installation du script
----------------------

On télécharge le script ``export-e3c.py`` et tout le sous-répertoire
``e3c/`` (le plus simple est de cloner le dépôt, ou d'en télécharger une
archive ZIP)

Les dépendances sont les suivantes :

*  Python3
*  les modules python3-odf (ou odfpy), bs4 (BeautifulSoup version 4)
*  une version récente de LibreOffice

Le script a été testé sur un système Linux, il doit pouvoir fonctionner
avec d'autres systèmes sans beaucoup de modification.

Test du fonctionnement
----------------------

Supposons que le ZIP du projet a été décompressé, et que le
répertoire courant est celui qui contient le script ``export-e3c.py``

Voici deux « copies d'écran »

### un fonctionnement correct ###

```bash
e3c-utils$ python3 export-e3c.py odt-files/e3c-modele.odt 
on a écrit et rendu interactif : out/e3c-modele.html
e3c-utils$
```

Dans ce cas, des fichiers sont apparus dans le sous-dossier ``out/``

```bash
e3c-utils$ ls out/
e3c-modele.html                       e3c-modele_html_d25385685b46351.gif
e3c-modele_html_2427e03aadfb2e0d.gif  e3c-modele_html_da43937f7d780718.gif
e3c-modele_html_78246a86c7fc5a90.gif  e3c-modele_html_ed2a6dc98caa7a82.gif
e3c-modele_html_90148ee3b7df4bcd.gif  e3c-modele_html_f152aa622140d924.gif
e3c-modele_html_c6a446140d9f0ae9.png  e3c-modele_html_f2d89dd50ea6b140.gif
e3c-modele_html_c773cd4ee885d8ab.gif  e3c-modele.odt
e3c-utils$
```

Dans ce sous-dossier, le fichier ``e3c-modele.odt`` est un OpenDocument
où on peut se rendre compte de tout ce qui a été ôté lors de la reconnaissance
des styles ; le fichier ``e3c-modele.html`` est une page web interactive
qui permet de « jouer avec le questionnaire ». Tous les fichiers commençant
par ``e3c-modele_html_...`` sont des images qui correspondent à des éléments
insérés, soit de façon explicite comme une image du document original, soit
implicitement, car LibreOffice convertit les inserts de texte, les formules
mathématiques et divers éléments en images lors de l'export vers HTML.

### un fonctionnement incorrect, détection d'une faute par le validateur ###

Voici un exemple de détection de faute :

```bash
e3c-utils$ python3 export-e3c.py odt-files/e3c-modele-erreur.odt 
>>>>>>>>>>>>>>>>>>> erreur <<<<<<<<<<<<<<<<<<<<<<<
Un thème ne doit pas survenir tant qu'on n'a pas fini de définir un QCM : « Représentation des données : types et valeurs de base »
ajoutez l'option -v pour augmenter la verbosité si vous voulez
e3c-utils$ 
```

Le message peut devenir plus explicite avec l'option verbeuse :

```bash
e3c-utils$ python3 export-e3c.py odt-files/e3c-modele-erreur.odt -v
>>>>>>>>>>>>>>>>>>> erreur <<<<<<<<<<<<<<<<<<<<<<<
Un thème ne doit pas survenir tant qu'on n'a pas fini de définir un QCM : « Représentation des données : types et valeurs de base »
>>>>>>>>>>>>>>>> texte déjà analysé <<<<<<<<<<<<<<


NSI-E3C
Modèle OpenDocument pour des QCM validables dans leur forme, et possibles à retravailler automatiquement.On peut travailler des façon suivantes : partir d’un exemple et substituer son contenudupliquer un exemple est en changer le contenuUtiliser avec discernement les styles spéciaux pour un nouveau QCM, et respecter certains mots-clés.Il faut respecter les contraintes suivantes :une question de QCM doit toujours suivre un titre principal ayant pour style QCM-Theme (voir le menu déroulant des styles), c’est obligatoire. On ne reformule pas les textes des thèmes du programme de NSI.une question de QCM peut arriver après un sous-thème (style QCM-sous-theme), c’est facultatif.L’énoncé d’une question de QCM doit avoir le style La liste des réponses doit être une liste numérotée, et tous les items doivent être de style CM-reponse ; les réponses sont relatives aux paragraphes précédents de style QCM-enonceLes réponses doivent être suivi par une ligne de style QCM-metadonnees. Cette ligne doit contenir une phrase sur le modèle :OK = <chiffre de 1 à 4>, niveau = {facile ou moyen ou difficile}Il n’est pas nécessaire de renseigner le niveau.Tout ce que l’on peut écrire dans le document, qui n’est pas marqué par les styles QCM-* est ignoré par les traitements automatiques 


Représentation des données : types et valeurs de base
Représentation binaire d’un entier relatif

Représentation approximative des nombres réels : notion de nombre flottant

Valeurs booléennes : 0, 1. Opérateurs booléens : and, or, not. Expressions booléennes
Expression A : not(0 and 1) or 0
Expression B : not((0 or 1) and 0)
A et B ont des valeurs booléennes opposéesA est vraieB est fausseces expression contiennent des fautes de syntaxe
OK = 2, niveau = moyen
Ceci est un commentaire : on utilise ci-dessus des valeurs 0 et 1 pour Faux et Vrai (ou True, False), comme écrit dans le programme au BO.
Représentation d’un texte en machine. Exemples des encodages ASCII, ISO-8859-1, Unicode
Dans une page web française, on lit à un emplacement donné :
DÃ©solÃ© !
e3c-utils$
```

Bogues connues
==============

Images trop petites
-------------------

L'exportation des boîtes de texte vers HTML produit des images, où la taille
des fontes est trop petite. Il faut recommander de placer une taille de police
150 % plus grande dans les boîtes de texte que dans le texte ordinaire, pour que
la lisibilité des pages web soit convenable.

Ce problème n'arrive pas avec les formules exportés comme images.
