#! /usr/bin/python3

import sys, argparse
from e3c.qcm import *

if __name__=="__main__":
    ##########################################################
    parser = argparse.ArgumentParser()
    parser.add_argument("odtFileName", help="Nom d'un fichier texte OpenDocument")
    parser.add_argument("-v", "--verbose", help="Force un rapport verbeux",
                        action="store_true")
    parser.add_argument("--to-html", help="Crée un fichier HTML interactif (si les QCM sont valides)",
                        action="store_true")
    args = parser.parse_args()
    ##########################################################
    doc = odf.opendocument.load(args.odtFileName)
    OK, lesQCM, deja, message = recolteQCM(doc)
    if OK and args.to_html:
        toHtml(lesQCM, doc, args.odtFileName)
    if OK and args.verbose:
        for theme, qs in lesQCM.items():
            print(f">>>>>>>>>>>>>>>>>>> {theme} <<<<<<<<<<<<<<<<<<<<<<<")
            for qcm in qs:
                print(qcm)
    elif OK and not args.verbose:
        qs=sum([q for t,q in lesQCM.items()],[])
        sys.stderr.write(f"Le fichier {args.odtFileName} contient {len(qs)} QCM valides\n")
    else:
        sys.stderr.write(">>>>>>>>>>>>>>>>>>> erreur <<<<<<<<<<<<<<<<<<<<<<<\n")
        sys.stderr.write(message+"\n")
        if args.verbose:
            sys.stderr.write(">>>>>>>>>>>>>>>> texte déjà analysé <<<<<<<<<<<<<<\n")
            for l in deja:
                sys.stderr.write(l+"\n")
        else:
            sys.stderr.write("ajouez l'option -v pour agmenter la verbosité si vous voulez\n")

